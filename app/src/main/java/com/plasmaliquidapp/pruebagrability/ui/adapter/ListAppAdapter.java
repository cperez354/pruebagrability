package com.plasmaliquidapp.pruebagrability.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plasmaliquidapp.pruebagrability.R;
import com.plasmaliquidapp.pruebagrability.domain.Application;
import com.plasmaliquidapp.pruebagrability.io.utilities.ClassApplication;
import com.plasmaliquidapp.pruebagrability.ui.activity.DetailApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cristian on 20/12/15.
 */
public class ListAppAdapter extends RecyclerView.Adapter<ListAppAdapter.ListAppViewHolder> {

    public Context context;
    public ArrayList<Application> apps;
    private Gson gson;

    /**
     * Parameter, Class used for save information in local storage.
     */
    private ClassApplication app;

    public ListAppAdapter(Context context) {
        this.context = context;
        this.apps = new ArrayList<>();
        this.app = (ClassApplication) this.context.getApplicationContext();
        this.gson = new Gson();
    }

    @Override
    public ListAppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.item_app, parent, false);

        return new ListAppViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ListAppViewHolder holder, int position) {
        Application currentApp = apps.get(position);
        holder.setAppName(currentApp.getName());

        if (currentApp.getImages() != null)
            holder.setImage(currentApp.getImages().get(2).getLink());
        else
            holder.setDefaultImage();
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public void addAll(@Nullable ArrayList<Application> apps) {

        if (apps == null)
            throw new NullPointerException("The categories collection cannot be null");

        this.apps = new ArrayList<>();
        this.apps.addAll(apps);

        notifyDataSetChanged();
    }

    public class ListAppViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView appName;
        ImageView appImg;


        public ListAppViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            appName = (TextView) itemView.findViewById(R.id.txt_name);
            appImg = (ImageView) itemView.findViewById(R.id.app_img);

        }

        public void setAppName(String name) {
            if (name.length() > 15)
                this.appName.setText(name.substring(0, 15) + "...");
            else
                this.appName.setText(name);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, DetailApplication.class);
            intent.putExtra("indexAppSelect", getAdapterPosition());
            intent.putExtra("appSelect", gson.toJson(apps.get(getAdapterPosition())));
            context.startActivity(intent);
        }

        public void setDefaultImage() {
            Picasso.with(context)
                    .load(R.drawable.ic_icon)
                    .into(appImg);
        }

        public void setImage(String urlImage) {
            Picasso.with(context)
                    .load(urlImage)
                    .placeholder(R.drawable.ic_icon)
                    .into(appImg);
        }
    }

}

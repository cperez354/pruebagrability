package com.plasmaliquidapp.pruebagrability.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plasmaliquidapp.pruebagrability.R;
import com.plasmaliquidapp.pruebagrability.domain.Category;
import com.plasmaliquidapp.pruebagrability.io.utilities.ClassApplication;
import com.plasmaliquidapp.pruebagrability.ui.activity.ListApplicationsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cristian on 20/12/15.
 */
public class ListCategoryAdapter extends RecyclerView.Adapter<ListCategoryAdapter.ListCategoryViewHolder> {

    public Context context;
    public ArrayList<Category> categories;
    public Gson gson;
    public int mode;

    /**
     * Parameter, Class used for save information in local storage.
     */
    private ClassApplication app;

    public ListCategoryAdapter(Context context) {
        this.context = context;
        this.categories = new ArrayList<>();
        this.app = (ClassApplication) this.context.getApplicationContext();
        this.gson = new Gson();
        this.mode = 1;
    }

    @Override
    public ListCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;

        if (this.mode == 1) {
            itemView = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);
        } else {
            itemView = LayoutInflater.from(context).inflate(R.layout.item_category_grid, parent, false);
        }

        return new ListCategoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ListCategoryViewHolder holder, int position) {
        Category currentCategory = categories.get(position);
        holder.setTestName(currentCategory.getName());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void addAll(@Nullable ArrayList<Category> categories) {

        if (categories == null)
            throw new NullPointerException("The categories collection cannot be null");

        this.categories = new ArrayList<>();
        this.categories.addAll(categories);

        notifyDataSetChanged();
    }

    public class ListCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView testName;


        public ListCategoryViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            testName = (TextView) itemView.findViewById(R.id.txt_name);

        }

        public void setTestName(String name) {
            this.testName.setText(name);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ListApplicationsActivity.class);
            intent.putExtra("indexCategorySelect", getAdapterPosition());
            intent.putExtra("categorySelect", gson.toJson(categories.get(getAdapterPosition())));
            context.startActivity(intent);
        }


    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}

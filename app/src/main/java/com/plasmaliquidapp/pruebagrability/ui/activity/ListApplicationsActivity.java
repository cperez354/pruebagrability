package com.plasmaliquidapp.pruebagrability.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plasmaliquidapp.pruebagrability.R;
import com.plasmaliquidapp.pruebagrability.domain.Category;
import com.plasmaliquidapp.pruebagrability.io.StoreApiAdapter;
import com.plasmaliquidapp.pruebagrability.io.responses.CategoryResponse;
import com.plasmaliquidapp.pruebagrability.io.utilities.ClassApplication;
import com.plasmaliquidapp.pruebagrability.io.utilities.Utilities;
import com.plasmaliquidapp.pruebagrability.ui.adapter.ListAppAdapter;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListApplicationsActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public Category categorySelect;
    public int indexCategorySelect;
    public Gson gson;
    public ClassApplication app;
    public RecyclerView app_list;
    public ListAppAdapter adapter;
    public ProgressBar progressBar;
    public DisplayMetrics displaymetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_applications);
        this.initComponents();
    }

    public void initComponents() {

        this.displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        this.gson = new Gson();
        this.categorySelect = this.gson.fromJson(getIntent().getExtras().getString("categorySelect"), Category.class);
        this.indexCategorySelect = getIntent().getExtras().getInt("indexCategorySelect");
        this.app_list = (RecyclerView) findViewById(R.id.app_list);
        this.adapter = new ListAppAdapter(this);
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.progressBar.setVisibility(View.VISIBLE);
        this.app = (ClassApplication) getApplication();

        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle(this.categorySelect.getName());

        if (toolbar != null)
            setSupportActionBar(toolbar);

        this.toolbar.setNavigationIcon(R.drawable.ic_action_back);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (Utilities.haveNetworkConnection(this)) {
            this.getApps();
        } else {
            if (this.categorySelect.getApplacations() != null) {
                adapter.addAll(this.categorySelect.getApplacations());
                progressBar.setVisibility(View.GONE);
            } else {
                Toast.makeText(this, getResources().getString(R.string.message_connection), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        }
        this.setUpAppList();

    }


    private void setUpAppList() {
        if (this.displaymetrics.widthPixels < 700)
            this.app_list.setLayoutManager(new LinearLayoutManager(this, 1, false));
        else {
            this.app_list.setLayoutManager(new GridLayoutManager(this, (int) (this.displaymetrics.widthPixels / getResources().getInteger(R.integer.cart_width))));
        }
        this.app_list.setAdapter(adapter);
    }

    /**
     * Method, Used for register a new buyer
     */
    private void getApps() {

        final Activity context = this;

        StoreApiAdapter.getApiService().getAppications(
                this.categorySelect.getId(),
                new Callback<CategoryResponse>() {
                    @Override
                    public void success(CategoryResponse result, Response response) {
                        app.getStore().getCategories().get(indexCategorySelect).setApplacations(result.getCategory().getApplacations());
                        categorySelect = result.getCategory();
                        app.saveStore();
                        adapter.addAll(categorySelect.getApplacations());
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.showAlert(context, getResources().getString(R.string.txt_failure_meesage_title), getResources().getString(R.string.txt_failure_meesage));
                        progressBar.setVisibility(View.GONE);
                    }
                }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

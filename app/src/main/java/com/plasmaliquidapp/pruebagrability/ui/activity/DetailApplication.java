package com.plasmaliquidapp.pruebagrability.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plasmaliquidapp.pruebagrability.R;
import com.plasmaliquidapp.pruebagrability.domain.Application;
import com.plasmaliquidapp.pruebagrability.io.utilities.ClassApplication;
import com.squareup.picasso.Picasso;

public class DetailApplication extends AppCompatActivity {

    public Toolbar toolbar;
    public Application applicationSelect;
    public int indexApplicationSelect;
    public Gson gson;
    public ClassApplication app;
    public ImageView imageParalax;
    public TextView author;
    public TextView descriptionApp;
    public TextView release;
    public TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_application);
        this.initComponents();
    }

    public void initComponents(){
        this.gson = new Gson();
        this.applicationSelect = this.gson.fromJson(getIntent().getExtras().getString("appSelect"), Application.class);
        this.indexApplicationSelect = getIntent().getExtras().getInt("indexAppSelect");


        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle(this.applicationSelect.getName());

        if (toolbar != null)
            setSupportActionBar(toolbar);

        this.toolbar.setNavigationIcon(R.drawable.ic_action_back);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.imageParalax = (ImageView) findViewById(R.id.image_paralax);
        this.setImage(applicationSelect.getImages().get(2).getLink(), imageParalax);

        this.author = (TextView) findViewById(R.id.txt_author);
        this.author.setText(this.applicationSelect.getAuthor());

        this.descriptionApp = (TextView) findViewById(R.id.txt_description);
        this.descriptionApp.setText(this.applicationSelect.getDescription());

        this.release = (TextView) findViewById(R.id.txt_release);
        this.release.setText(this.applicationSelect.getReleaseDate());

        this.price = (TextView) findViewById(R.id.txt_price);
        this.price.setText(this.applicationSelect.getPrice());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_favorite:
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }


    public void setImage(String urlImage, ImageView container) {
        Picasso.with(this)
                .load(urlImage)
                .placeholder(R.drawable.ic_icon)
                .into(container);
    }

}

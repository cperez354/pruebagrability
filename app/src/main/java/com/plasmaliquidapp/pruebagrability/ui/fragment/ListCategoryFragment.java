package com.plasmaliquidapp.pruebagrability.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.plasmaliquidapp.pruebagrability.R;
import com.plasmaliquidapp.pruebagrability.io.utilities.ClassApplication;
import com.plasmaliquidapp.pruebagrability.ui.adapter.ListCategoryAdapter;


/**
 * A placeholder fragment containing a simple view.
 */
public class ListCategoryFragment extends Fragment {

    public View rootView;
    public ClassApplication app;
    public DisplayMetrics displaymetrics;
    public RecyclerView listCalc;
    public ListCategoryAdapter adapter;
    public ProgressBar progressBar;

    public ListCategoryFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adapter = new ListCategoryAdapter(getActivity());
        this.app = (ClassApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.rootView = inflater.inflate(R.layout.fragment_list_category, container, false);
        this.progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        this.initComponents();

        return rootView;
    }

    public void initComponents() {

        this.displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        this.listCalc = (RecyclerView) rootView.findViewById(R.id.category_list);
        this.progressBar.setVisibility(View.VISIBLE);
        this.adapter.addAll(this.app.getStore().getCategories());
        this.progressBar.setVisibility(View.GONE);
        this.setUpAppList();

    }

    private void setUpAppList() {
        if (this.displaymetrics.widthPixels < 700) {
            this.adapter.setMode(1);
            this.listCalc.setLayoutManager(new LinearLayoutManager(getActivity(), 1, false));
        }
        else{
            this.adapter.setMode(2);
            this.listCalc.setLayoutManager(new GridLayoutManager(getActivity(), (int)(this.displaymetrics.widthPixels/getResources().getInteger(R.integer.cart_width))));
        }
        this.listCalc.setAdapter(adapter);
    }


}

package com.plasmaliquidapp.pruebagrability.domain;

/**
 * Created by Cristian on 3/20/16.
 */
public class Img {

    public String link;
    public String height;

    public Img() {
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}

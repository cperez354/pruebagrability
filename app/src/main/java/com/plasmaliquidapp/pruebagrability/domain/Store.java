package com.plasmaliquidapp.pruebagrability.domain;

import java.util.ArrayList;

/**
 * Created by Cristian on 3/20/16.
 */
public class Store {

    public ArrayList<Category> categories;

    public Store() {
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void completeCategories(){

        this.categories = new ArrayList<>();

        String [] arrayNames = ("Books,Business,Catalogs,Education,Entertainment," +
                                "Finance,Food and drink,Games,Health and Fitness," +
                                "Lifestyle,Medical,Music,Navigation,News,Newsstand," +
                                "Photo and Video,Productivity,Reference,Social Networking," +
                                "Sports,Travel,Utilities,Weather").split(",");

        String [] arrayId = ("6018,6000,6022,6017,6016,6015,6023,6014,6013,6012,6020,6011," +
                             "6010,6009,6021,6008,6007,6006,6005,6004,6003,6002,6001").split(",");

        for (int i = 0; i < arrayId.length; i++) {
            Category aux = new Category();
            aux.setId(arrayId[i]);
            aux.setName(arrayNames[i]);
            this.categories.add(aux);
        }
    }
}

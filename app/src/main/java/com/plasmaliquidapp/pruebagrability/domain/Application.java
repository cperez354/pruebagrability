package com.plasmaliquidapp.pruebagrability.domain;

import java.util.ArrayList;

/**
 * Created by Cristian on 3/20/16.
 */
public class Application {

    public String name;
    public String description;
    public String price;
    public String author;
    public String releaseDate;
    public String category;
    public String idCategory;
    public ArrayList<Img> images;

    public Application() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public ArrayList<Img> getImages() {
        return images;
    }

    public void setImages(ArrayList<Img> images) {
        this.images = images;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }
}

package com.plasmaliquidapp.pruebagrability.domain;

import java.util.ArrayList;

/**
 * Created by Cristian on 3/20/16.
 */
public class Category {

    public String id;
    public String name;
    public ArrayList<Application> applacations;

    public Category() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Application> getApplacations() {
        return applacations;
    }

    public void setApplacations(ArrayList<Application> applacations) {
        this.applacations = applacations;
    }
}

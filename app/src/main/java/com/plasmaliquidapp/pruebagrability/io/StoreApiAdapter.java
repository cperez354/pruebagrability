package com.plasmaliquidapp.pruebagrability.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.plasmaliquidapp.pruebagrability.io.constants.ApiConstants;
import com.plasmaliquidapp.pruebagrability.io.deserializer.CategoryDeserializer;
import com.plasmaliquidapp.pruebagrability.io.responses.CategoryResponse;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Cristian on 3/20/16.
 */
public class StoreApiAdapter {

    public static StoreApiService API_SERVICE;

    public static StoreApiService getApiService(){

        if(API_SERVICE == null){

            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(ApiConstants.URL_BASE)
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .setConverter(buildIntegraGsonConverter())
                    .build();

            API_SERVICE = adapter.create(StoreApiService.class);
        }

        return API_SERVICE;
    }

    private static GsonConverter buildIntegraGsonConverter(){

        Gson gsonConf = new GsonBuilder()
                .registerTypeAdapter(CategoryResponse.class,new CategoryDeserializer())
                .create();

        return new GsonConverter(gsonConf);

    }
}

package com.plasmaliquidapp.pruebagrability.io.responses;

import com.google.gson.annotations.SerializedName;
import com.plasmaliquidapp.pruebagrability.domain.Category;
import com.plasmaliquidapp.pruebagrability.io.constants.JsonKeys;

/**
 * Created by Cristian on 20/12/15.
 */
public class CategoryResponse {


    @SerializedName(JsonKeys.STORE)
    Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}

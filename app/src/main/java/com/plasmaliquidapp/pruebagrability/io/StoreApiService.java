package com.plasmaliquidapp.pruebagrability.io;

import com.plasmaliquidapp.pruebagrability.io.constants.ApiConstants;
import com.plasmaliquidapp.pruebagrability.io.responses.CategoryResponse;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by Cristian on 3/20/16.
 */
public interface StoreApiService {

    @GET("/genre={id}/json")
    void getAppications(@Path("id") String id,
                        Callback<CategoryResponse> serverResponse);

}

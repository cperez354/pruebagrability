package com.plasmaliquidapp.pruebagrability.io.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.plasmaliquidapp.pruebagrability.R;

import java.lang.reflect.Field;

/**
 * Created by Cristian on 2/16/16.
 */
public class Utilities {

    public static void setDefaultFont(Context context,
                                      String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }
    protected static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void showAlert(Activity activity, String title, String message){
        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setTitle(title);
        adb.setMessage(message)
                .setCancelable(false)
                .setPositiveButton( activity.getResources().getString(R.string.btn_acept),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        adb.show();
    }

    public static boolean haveNetworkConnection(Activity act) {

        ConnectivityManager connectivity = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo=connectivity.getActiveNetworkInfo();
        if (nInfo != null && nInfo.getState()== NetworkInfo.State.CONNECTED) {
            return true;
        }
        return false;
    }

}
package com.plasmaliquidapp.pruebagrability.io.constants;

/**
 * Created by Cristian on 3/20/16.
 */
public class JsonKeys {


    //GLOBAL KEYS
    public static final String STORE = "feed";
    public static final String LABEL = "label";
    public static final String HEIGHT = "height";
    public static final String ATTRIBUTES = "attributes";
    public static final String APPLICATIONS = "entry";


    //APPLICATIONS KEYS
    public static final String APPLICATION_NAME = "im:name";
    public static final String APPLICATION_DESCRIPTION = "summary";
    public static final String APPLICATION_PRICE = "im:price";
    public static final String APPLICATION_AUTOR = "im:artist";
    public static final String APPLICATION_RELEASE_DATE = "im:releaseDate";
    public static final String APPLICATION_IMAGES = "im:image";
    public static final String APPLICATION_CATEGORY = "category";
    public static final String APPLICATION_CATEGORY_NAME = "term";
    public static final String APPLICATION_CATEGORY_ID = "im:id";
}

package com.plasmaliquidapp.pruebagrability.io.utilities;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.plasmaliquidapp.pruebagrability.domain.Category;
import com.plasmaliquidapp.pruebagrability.domain.Store;

import java.util.ArrayList;

/**
 * Created by Cristian on 2/16/16.
 */
public class ClassApplication extends Application {

    private Store store;
    private SharedPreferences pref;
    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        this.gson = new Gson();
        this.pref = getSharedPreferences("pref", MODE_PRIVATE);
        this.store = this.gson.fromJson(pref.getString("store","{}"),Store.class);
        Utilities.setDefaultFont(this, "MONOSPACE", "fonts/BebasNeue.otf");

    }

    public Store getStore() {
        this.store = this.gson.fromJson(pref.getString("store","{}"),Store.class);

        if(this.store.getCategories() !=null){
            return this.store;
        }
        else{
            this.store.completeCategories();
            this.setStore(this.store);
            return this.store;
        }
    }

    public void setStore(Store store) {
        SharedPreferences.Editor editor = this.pref.edit();
        editor.putString("store", gson.toJson(store));
        editor.commit();
        this.store = store;
    }

    public void saveStore(){
        SharedPreferences.Editor editor = this.pref.edit();
        editor.putString("store", gson.toJson(this.store));
        editor.commit();
    }

}

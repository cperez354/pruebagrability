package com.plasmaliquidapp.pruebagrability.io.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.plasmaliquidapp.pruebagrability.domain.Application;
import com.plasmaliquidapp.pruebagrability.domain.Category;
import com.plasmaliquidapp.pruebagrability.domain.Img;
import com.plasmaliquidapp.pruebagrability.io.constants.JsonKeys;
import com.plasmaliquidapp.pruebagrability.io.responses.CategoryResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Cristian on 3/20/16.
 */
public class CategoryDeserializer implements JsonDeserializer<CategoryResponse> {


    @Override
    public CategoryResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Gson gson = new Gson();
        CategoryResponse response = null;

        try{
            response = gson.fromJson(json, CategoryResponse.class);
        }
        catch (Exception e){
            e.printStackTrace();
            response = new CategoryResponse();
        }

        Category category = new Category();

        //Obtener el objeto feed
        JsonObject feedObject = json.getAsJsonObject().getAsJsonObject(JsonKeys.STORE);

        //Obtenemos el array entry del json y lo convertimos en arrayList
        ArrayList<Application> applications = getApplicationsFromJsonArray(feedObject.get(JsonKeys.APPLICATIONS).getAsJsonArray());


        category.setId(applications.get(0).getIdCategory());
        category.setName(applications.get(0).getCategory());
        category.setApplacations(applications);

        response.setCategory(category);

        return response;
    }

    private ArrayList<Application> getApplicationsFromJsonArray(JsonArray applications){

        ArrayList<Application> applicationsAux = new ArrayList();

        for (int i = 0; i < applications.size(); i++ ) {

            Application aux = new Application();
            JsonObject TestData = applications.get(i).getAsJsonObject();

            aux.setName(TestData.get(JsonKeys.APPLICATION_NAME).getAsJsonObject().get(JsonKeys.LABEL).getAsString());
            aux.setDescription(TestData.get(JsonKeys.APPLICATION_DESCRIPTION).getAsJsonObject().get(JsonKeys.LABEL).getAsString());
            aux.setPrice(TestData.get(JsonKeys.APPLICATION_PRICE).getAsJsonObject().get(JsonKeys.LABEL).getAsString());
            aux.setAuthor(TestData.get(JsonKeys.APPLICATION_AUTOR).getAsJsonObject().get(JsonKeys.LABEL).getAsString());
            aux.setReleaseDate(TestData.get(JsonKeys.APPLICATION_RELEASE_DATE).getAsJsonObject().get(JsonKeys.ATTRIBUTES).getAsJsonObject().get(JsonKeys.LABEL).getAsString());
            aux.setCategory(TestData.get(JsonKeys.APPLICATION_CATEGORY).getAsJsonObject().get(JsonKeys.ATTRIBUTES).getAsJsonObject().get(JsonKeys.APPLICATION_CATEGORY_NAME).getAsString());
            aux.setIdCategory(TestData.get(JsonKeys.APPLICATION_CATEGORY).getAsJsonObject().get(JsonKeys.ATTRIBUTES).getAsJsonObject().get(JsonKeys.APPLICATION_CATEGORY_ID).getAsString());

            aux.setImages(getImagesFromJsonArray(TestData.get(JsonKeys.APPLICATION_IMAGES).getAsJsonArray()));

            applicationsAux.add(aux);
        }

        return applicationsAux;

    }

    private ArrayList<Img> getImagesFromJsonArray(JsonArray images){

        ArrayList<Img> imagesAux = new ArrayList();

        for (int i = 0; i < images.size(); i++ ) {

            Img aux = new Img();
            JsonObject TestData = images.get(i).getAsJsonObject();

            aux.setLink(TestData.get(JsonKeys.LABEL).getAsString());
            aux.setHeight(TestData.get(JsonKeys.ATTRIBUTES).getAsJsonObject().get(JsonKeys.HEIGHT).getAsString());

            imagesAux.add(aux);
        }

        return imagesAux;

    }


}
